from django.db import models


class Root(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name


class Node(models.Model):
    name = models.CharField(max_length=10)
    root = models.ForeignKey(Root, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
