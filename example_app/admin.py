from django.contrib import admin

from example_app.forms import RootAdminForm
from example_app.models import *


@admin.register(Node)
class SymbolAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'root']


@admin.register(Root)
class RootAdmin(admin.ModelAdmin):
    list_display = ['name']

    form = RootAdminForm
