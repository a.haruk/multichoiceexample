from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple

from example_app.models import Node, Root


# https://stackoverflow.com/questions/59302784/django-modelmultiplechoicefield-lazy-loading-of-related-m2m-objects
class RootAdminForm(forms.ModelForm):
    class Meta:
        model = Root
        exclude = []

    node_select = forms.ModelMultipleChoiceField(
        queryset=Node.objects.all(),
        required=False,
        widget=FilteredSelectMultiple('node_select', False)
    )

    def __init__(self, *args, **kwargs):
        super(RootAdminForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['node_select'].initial = self.instance.node_set.all()

    def _save_m2m(self):
        save = super(RootAdminForm, self)._save_m2m()
        self.instance.node_set.set(self.cleaned_data['node_select'])
        return save

    def save(self, commit=True):
        instance = super(RootAdminForm, self).save(commit)
        self._save_m2m()
        return instance
